# Software Studio 2020 Spring
## Assignment 01 Web Canvas
## 107062112


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
|solid shape                                   | 1~5%     | Y         |


---

### How to use

#### pencil
click once then you can draw line
![](https://i.imgur.com/J91myPr.png)

#### eraser
click once then you can get a eraser
![](https://i.imgur.com/FrXsRGq.png)

#### brush size
click the options to change the brush size
![](https://i.imgur.com/DPBx1PT.png)

#### text
click once then you can draw text
![](https://i.imgur.com/wNdHnOm.png)

#### text font
you can sekect text font
![](https://i.imgur.com/cPKuwOe.png)

#### circle
toy can draw a hollow circle
![](https://i.imgur.com/MsFLjml.png)

#### square
you can draw a hollow square
![](https://i.imgur.com/VxSLKtx.png)

#### triangle
you can draw a hollow triangle
![](https://i.imgur.com/DnrIxg4.png)

#### download
you can download what you draw on canva as a png
![](https://i.imgur.com/rASENoe.png)

#### upload
you can upload a image onto canva
![](https://i.imgur.com/yUGfRiA.png)

#### refresh
you can make the canva clean
![](https://i.imgur.com/87iUzXy.png)

#### undo
you can return to previous state
![](https://i.imgur.com/CKQzlBC.png)

#### redo
you can return to next state
![](https://i.imgur.com/lfjlzdW.png)

#### color change
you can change the color on brush, shape, ...
![](https://i.imgur.com/gxcho8v.png)

#### solid
click once you can make the shape become solid
one more click can make the shape become hollow
![](https://i.imgur.com/N3At4id.png)


### Function description

#### solid
```javascript=1
c.fill();
```
I use this function to make solid shape


### Gitlab page link

"https://107062112.gitlab.io/AS_01_WebCanvas"


<style>
table th{
    width: 100%;
}
</style>